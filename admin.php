<?php 
	include 'header.php';
?>

<body>
	<div class="container">
       <div class="jumbotron">
            <h1>Web VP</h1>
            <p>Bienvenue sur compte Admin</p>
        </div>
		<div class="row">
			<ul class="nav nav-pills nav-stacked">
				<li role="presentation"><a href="views/manageUsers.php">Gestion des utilisateurs</a></li>
				<li role="presentation"><a href="views/manageTickets.php">Gestion des tickets</a></li>
				<li role="presentation"><a href="views/manageClients.php">Gestion des clients</a></li>
				<li role="presentation"><a href="views/manageProjects.php">Gestion des projets</a></li>
				<li role="presentation" class="active"><a href="#">Déconnexion</a></li>
			</ul>
		</div>
	</div>
	<?php 
		include 'footer.php';
	?>