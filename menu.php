<?php 
    function clientMenu(){
        echo "<ul class=\"nav nav-pills nav-stacked\">";
        echo "<li role=\"presentation\"><a href=\"manageClients.php\">Gestion des clients</a></li>";
        echo "<li role=\"presentation\"><a href=\"addClient.php\">Ajouter un client</a></li>";
        echo "<li role=\"presentation\"><a href=\"updateClient.php\">Modifier un client</a></li>";
        echo "<li role=\"presentation\"><a href=\"deleteClient.php\">Supprimer des clients</a></li>";
        echo "</ul>";
    }
    
    function projectMenu(){
        echo "<ul class=\"nav nav-pills nav-stacked\">";
        echo "<li role=\"presentation\"><a href=\"manageProjects.php\">Gestion des projets</a></li>";
        echo "<li role=\"presentation\"><a href=\"addProject.php\">Ajouter un projet</a></li>";
        echo "<li role=\"presentation\"><a href=\"updateProject.php\">Modifier un projet</a></li>";
        echo "<li role=\"presentation\"><a href=\"deleteProject.php\">Supprimer des projets</a></li>";
        echo "</ul>";
    }
    
    function userMenu(){
        echo "<ul class=\"nav nav-pills nav-stacked\">";
        echo "<li role=\"presentation\"><a href=\"manageUsers.php\">Gestion des utilisateurs</a></li>";
        echo "<li role=\"presentation\"><a href=\"addUser.php\">Ajouter un utilisateur</a></li>";
        echo "<li role=\"presentation\"><a href=\"updateUser.php\">Modifier un utilisateur</a></li>";
        echo "<li role=\"presentation\"><a href=\"deleteUser.php\">Supprimer des utilisateurs</a></li>";
        echo "</ul>";
    }
    
    function ticketMenu(){
        echo "<ul class=\"nav nav-pills nav-stacked\">";
        echo "<li role=\"presentation\"><a href=\"manageTickets.php\">Gestion des tickets</a></li>";
        echo "<li role=\"presentation\"><a href=\"createTicket.php\">Créer un ticket</a></li>";
        echo "<li role=\"presentation\"><a href=\"updateTicketState.php\">Modifier le statut d'un ticket</a></li>";
        echo "<li role=\"presentation\"><a href=\"ticketHistoric.php\">Consulter l'historique d'un ticket</a></li>";
        echo "</ul>";
    }
?>