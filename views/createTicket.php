<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/ticketsController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Création d'un ticket</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php ticketMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="createTicket.php" method="post">					
					<p>Veuillez compléter chaque champ pour créer un ticket</p>	
					<div class="form-group">
						<label for="details">Détails</label>
						<input type="text" class="form-control" id="details" name="details" placeholder="Détails">
						<label for="signalement">Signalement</label>
						<select name="signalement" id="signalement">				
            				<option name="signalement" value=""></option>						
            				<option name="signalement" value="Mail">Mail</option>
            				<option name="signalement" value="Telephone">Téléphone</option>
						</select><br>
						<label for="rapporteur">Rapporteur</label>
        				<select name="rapporteur" id="rapporteur">
        					<?php displayRapporteursDropdownList(); ?>
        				</select><br>
                        <label for="client">Client</label>
                        <select name="client" id="client">
                        	<?php displayClientsDropdownList(); ?>
                        </select><br>
                        <label for="project">Projet</label>
                        <select name="project" id="project">
                        	<?php displayProjectsDropdownList(); ?>
                        </select><br>
					</div>
					<button type="submit" class="btn btn-default">Ajouter</button>
				</form>
				<br><br>
				<form action="manageTickets.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_POST['details']) && isset($_POST['signalement']) && isset($_POST['rapporteur']) && isset($_POST['client']) && isset($_POST['project'])){
				createTicket($_POST['details'], $_POST['signalement'], $_POST['rapporteur'], $_POST['client'], $_POST['project']);
				header('Location: manageTickets.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>