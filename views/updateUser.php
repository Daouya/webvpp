<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/usersController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Modification d'un utilisateur</p>
		</div>		
		<div class="row">
			<div class="col-md-3">
				<?php userMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="updateUser.php" method=post>
					<div class="form-group">
						<label for="depart">Sélectionnez l'utilisateur &agrave; modifier : </label>
						<select name="users" id="users">
							<?php displayDropdownList(); ?>
						</select>
					</div>					
					<button type="submit" class="btn btn-default">Valider</button>
				</form>
				<br>
				<form action="updateUser.php" method="post">
					<div class="form-group">
						<p>Champ àmodifier :</p>
						<?php
							if(isset($_POST['users']) && $_POST['users'] != " "){
								$_SESSION['users'] = $_POST['users'];
								displayUpdateForm($_SESSION['users']);
							}else{		
							    displayUserForm("disabled", "disabled", "disabled", "disabled", "disabled", "disabled", false);
							}
						?>
					</div>
					<button type="submit" class="btn btn-default">Modifier</button>
				</form>
				<br>
				<form action="manageUsers.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_SESSION['users']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['type']) && isset($_POST['password'])){
		        modifyUser($_POST['nom'], $_POST['prenom'], $_POST['type'], $_POST['password'], $_SESSION['users']);
				header('Location: manageUsers.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>