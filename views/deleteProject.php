<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/projectsController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Suppression d'un projet</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php projectMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Choisissez le ou les projet(s) &agrave; supprimer</p>
				<form action="deleteProject.php" method="post">
					<div class="form-group">
						<?php 
						    displayProjectsCheckbox();
						?>
					</div>
					<button type="submit" class="btn btn-default">Supprimer</button>
				</form>
				<br><br>
				<form action="manageProjects.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
			if(isset($_POST['project'])){
			    removeProject($_POST['project']);
				header('Location: manageProjects.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>