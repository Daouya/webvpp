<?php
    include '../header.php';
    include '../controllers/connexionController.php';
?>
		<p>Bienvenue sur votre outil de gestion d'incident</p>
		</div>
		<div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-9">
				<form action="connexion.php" method="post">									
					<div class="form-group">						
						<label for="login">Login</label>
						<input type="text" class="form-control" id="login" name="login" placeholder="Login">
					</div>
					<div class="form-group">
						<label for="passwd">Mot de passe</label>
						<input type="password" class="form-control" id="passwd" name="passwd" placeholder="Mot de passe">
					</div>
					<br></br>
					<?php
    					if (isset($_POST['login']) && isset($_POST['passwd'])){
    					    checkConnexion($_POST['login'],$_POST['passwd']);					    
    					}                    	
                    ?>					
					<button type="submit" class="btn btn-default">Connexion</button>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include '../footer.php';
	?>
	


  </body>
</html>	
  
