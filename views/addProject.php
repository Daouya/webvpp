<?php
    include '../header.php';
    include '../menu.php';
    include '../controllers/projectsController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Ajout d'un projet</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php projectMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="addProject.php" method="post">					
					<p>Veuillez compléter chaque champ pour ajouter un projet</p>	
					<div class="form-group">
						<label for="nom">Nom</label>
						<input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
					</div>
					<button type="submit" class="btn btn-default">Ajouter</button>
				</form>
				<br><br>
				<form action="manageProjects.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_POST['nom'])){
				createProject($_POST['nom']);
				header('Location: manageProjects.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>