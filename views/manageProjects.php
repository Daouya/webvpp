<?php 
	include '../header.php';	
	include '../menu.php';
	include '../controllers/projectsController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Bienvenue sur votre espace de gestion des projets</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php projectMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Liste des projets :</p>
				<br>
				<table>
					<tr>
						<th class="col-md-1">Numéro</th>
						<th class="col-md-1">Nom</th>
					</tr>
					<?php displayAllProjects(); ?>
				</table>
				<br><br>
				<form action="../admin.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include '../footer.php';
	?>