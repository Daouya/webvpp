<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/projectsController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Modification d'un projet</p>
		</div>		
		<div class="row">
			<div class="col-md-3">
				<?php projectMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="updateProject.php" method=post>
					<div class="form-group">
						<label for="depart">Sélectionnez le projet &agrave; modifier : </label>
						<select name="projects" id="projects">
							<?php displayDropdownList(); ?>
						</select>
					</div>					
					<button type="submit" class="btn btn-default">Valider</button>
				</form>
				<br>
				<form action="updateProject.php" method="post">
					<div class="form-group">
						<p>Champ àmodifier :</p>
						<?php
							if(isset($_POST['projects']) && $_POST['projects'] != " "){
								$_SESSION['projects'] = $_POST['projects'];
								displayUpdateForm($_SESSION['projects']);
							}else{		
							    displayProjectForm("disabled", "disabled", "disabled", "disabled", "disabled", "disabled");
							}
						?>
					</div>
					<button type="submit" class="btn btn-default">Modifier</button>
				</form>
				<br>
				<form action="manageProjects.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_SESSION['projects']) && isset($_POST['nom'])){
		        modifyProject($_POST['nom'], $_SESSION['projects']);
				header('Location: manageProjects.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>