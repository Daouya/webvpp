<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/usersController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Suppression d'un utilisateur</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php userMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Choisissez le ou les utilisateur(s) &agrave; supprimer</p>
				<form action="deleteUser.php" method="post">
					<div class="form-group">
						<?php 
						    displayUsersCheckbox();
						?>
					</div>
					<button type="submit" class="btn btn-default">Supprimer</button>
				</form>
				<br><br>
				<form action="manageUsers.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
			if(isset($_POST['user'])){
			    removeUser($_POST['user']);
				header('Location: manageUsers.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>