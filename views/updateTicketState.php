<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/ticketsController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Mise &agrave; jour du statut d'un ticket</p>
		</div>		
		<div class="row">
			<div class="col-md-3">
				<?php ticketMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="updateTicketState.php" method=post>
					<div class="form-group">
						<label for="depart">Sélectionnez le ticket &agrave; mettre &agrave; jour : </label>
						<select name="tickets" id="tickets">
							<?php displayDropdownList(); ?>
						</select>
					</div>					
					<button type="submit" class="btn btn-default">Valider</button>
				</form>
				<br>
				<form action="updateTicketState.php" method="post">
					<div class="form-group">
						<p>Champ àmodifier :</p>
						<?php
							if(isset($_POST['tickets']) && $_POST['tickets'] != " "){
								$_SESSION['tickets'] = $_POST['tickets'];
								displayUpdateForm($_SESSION['tickets']);
							}else{		
							    displayTicketForm("disabled", "disabled", "disabled", "disabled");
							}
						?>
					</div>
					<button type="submit" class="btn btn-default">Modifier</button>
				</form>
				<br>
				<form action="manageTickets.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_SESSION['tickets']) && isset($_POST['statut']) && isset($_POST['commentaire']) && isset($_POST['developpeur'])){
		        modifyTicketState($_SESSION['tickets'], $_POST['statut'], $_POST['commentaire'], $_POST['developpeur']);
				header('Location: manageTickets.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>