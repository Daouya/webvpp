<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/clientsController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Modification d'un client</p>
		</div>		
		<div class="row">
			<div class="col-md-3">
				<?php clientMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="updateClient.php" method=post>
					<div class="form-group">
						<label for="depart">Sélectionnez le client &agrave; modifier : </label>
						<select name="clients" id="clients">
							<?php displayDropdownList(); ?>
						</select>
					</div>					
					<button type="submit" class="btn btn-default">Valider</button>
				</form>
				<br>
				<form action="updateClient.php" method="post">
					<div class="form-group">
						<p>Champ àmodifier :</p>
						<?php
							if(isset($_POST['clients']) && $_POST['clients'] != " "){
								$_SESSION['clients'] = $_POST['clients'];
								displayUpdateForm($_SESSION['clients']);
							}else{		
							    displayClientForm("disabled", "disabled", "disabled", "disabled", "disabled", "disabled");
							}
						?>
					</div>
					<button type="submit" class="btn btn-default">Modifier</button>
				</form>
				<br>
				<form action="manageClients.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_SESSION['clients']) && isset($_POST['nom']) && isset($_POST['adresse']) && isset($_POST['ville']) && isset($_POST['telephone']) && isset($_POST['mail'])){
		        modifyClient($_POST['nom'], $_POST['adresse'], $_POST['ville'], $_POST['telephone'], $_POST['mail'], $_SESSION['clients']);
				header('Location: manageClients.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>