<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/clientsController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Ajout d'un client</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php clientMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="addClient.php" method="post">					
					<p>Veuillez compléter chaque champ pour ajouter un client</p>	
					<div class="form-group">
						<label for="nom">Nom</label>
						<input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
						<label for="adresse">Adresse</label>
						<input type="text" class="form-control" id="adresse" name="adresse" placeholder="Adresse">
						<label for="ville">Ville</label>
						<input type="text" class="form-control" id="ville" name="ville" placeholder="Ville">
						<label for="telephone">Téléphone</label>
						<input type="text" class="form-control" id="telephone" name="telephone" placeholder="Téléphone">
						<label for="mail">Mail</label>
						<input type="text" class="form-control" id="mail" name="mail" placeholder="Mail">
					</div>
					<button type="submit" class="btn btn-default">Ajouter</button>
				</form>
				<br><br>
				<form action="manageClients.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_POST['nom']) && isset($_POST['adresse']) && isset($_POST['ville']) && isset($_POST['telephone']) && isset($_POST['mail'])){
				createClient($_POST['nom'], $_POST['adresse'], $_POST['ville'], $_POST['telephone'], $_POST['mail']);
				header('Location: manageClients.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>