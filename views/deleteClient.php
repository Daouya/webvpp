<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/clientsController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Suppression d'un client</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php clientMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Choisissez le ou les client(s) &agrave; supprimer</p>
				<form action="deleteClient.php" method="post">
					<div class="form-group">
						<?php 
						    displayClientsCheckbox();
						?>
					</div>
					<button type="submit" class="btn btn-default">Supprimer</button>
				</form>
				<br><br>
				<form action="manageClients.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
			if(isset($_POST['client'])){
			    removeClient($_POST['client']);
				header('Location: manageClients.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>