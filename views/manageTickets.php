<?php 
	include '../header.php';
	include '../menu.php';
	include '../controllers/ticketsController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Bienvenue sur votre espace de gestion des tickets</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php ticketMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Liste des tickets :</p>
				<br>
				<table>
					<tr>
						<th class="col-md-1">Numéro</th>
						<th class="col-md-1">Date de création</th>
						<th class="col-md-1">Détails</th>
						<th class="col-md-1">Signalement</th>
						<th class="col-md-1">Rapporteur</th>
						<th class="col-md-1">Client</th>
						<th class="col-md-1">Projet</th>
					</tr>
					<?php displayAllTickets(); ?>
				</table>
				<br><br>
				<form action="../admin.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include '../footer.php';
	?>