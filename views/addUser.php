<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/usersController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Ajout d'un utilisateur</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php userMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="addUser.php" method="post">					
					<p>Veuillez compléter chaque champ pour ajouter un utilisateur</p>	
					<div class="form-group">
						<label for="login">Login</label>
						<input type="text" class="form-control" id="login" name="login" placeholder="Login">
						<label for="nom">Nom</label>
						<input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
						<label for="prenom">Prénom</label>
						<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom">
						<label for="type">Type</label>
						<select name="type" id="type">							
            				<option name="type" value="Rapporteur">Rapporteur</option>
            				<option name="type" value="Developpeur">Développeur</option>
						</select><br>
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Password">
					</div>
					<button type="submit" class="btn btn-default">Ajouter</button>
				</form>
				<br><br>
				<form action="manageUsers.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
		<?php 
		    if(isset($_POST['login']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['type']) && isset($_POST['password'])){
		        createUser($_POST['login'], $_POST['nom'], $_POST['prenom'], $_POST['type'], $_POST['password']);
				header('Location: manageUsers.php');
			}
		?>
	</div>
	<?php 
		include '../footer.php';
	?>