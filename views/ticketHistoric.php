<?php 
    include '../header.php';
    include '../menu.php';
    include '../controllers/ticketsController.php';
?>

<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Consultation de l'historique d'un ticket</p>
		</div>		
		<div class="row">
			<div class="col-md-3">
				<?php ticketMenu(); ?>
			</div>
			<div class="col-md-9">
				<form action="ticketHistoric.php" method=post>
					<div class="form-group">
						<label for="depart">Sélectionnez le ticket &agrave; consulter : </label>
						<select name="tickets" id="tickets">
							<?php displayDropdownList(); ?>
						</select>
					</div>					
					<button type="submit" class="btn btn-default">Valider</button>
				</form>
				<br>
				<form action="ticketHistoric.php" method="post">
					<div class="form-group">
						<?php
							if(isset($_POST['tickets']) && $_POST['tickets'] != " "){
								$_SESSION['tickets'] = $_POST['tickets'];
						?>
						<table>
    						<tr>
        						<th class="col-md-1">Numéro</th>
        						<th class="col-md-1">Détails</th>
        						<th class="col-md-1">Signalement</th>
        						<th class="col-md-1">Client</th>
        						<th class="col-md-1">Projet</th>
    						</tr>
							<?php displayTicket($_SESSION['tickets']); ?>
						</table>
						<?php 
						        displayTicketHistoric($_SESSION['tickets']);
							}else{		
							    displayHistoric("disabled", "disabled", "disabled", "disabled");
							}
						?>
					</div>
				</form>
				<br>
				<form action="manageTickets.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include '../footer.php';
	?>