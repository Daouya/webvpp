<?php 
	include '../header.php';	
	include '../menu.php';
	include '../controllers/usersController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Bienvenue sur votre espace de gestion des utilisateurs</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php userMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Liste des utilisateurs :</p>
				<br>
				<table>
					<tr>
						<th class="col-md-1">Login</th>
						<th class="col-md-1">Nom</th>
						<th class="col-md-1">Prénom</th>
						<th class="col-md-1">Type</th>
					</tr>
					<?php displayAllUsers(); ?>
				</table>
				<br><br>
				<form action="../admin.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include '../footer.php';
	?>