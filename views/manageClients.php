<?php 
	include '../header.php';	
	include '../menu.php';
	include '../controllers/clientsController.php';
?>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>Web VP</h1>
			<p>Bienvenue sur votre espace de gestion des clients</p>
		</div>
		<div class="row">
			<div class="col-md-3">
				<?php clientMenu(); ?>
			</div>
			<div class="col-md-9">
				<p>Liste des clients :</p>
				<br>
				<table>
					<tr>
						<th class="col-md-1">Numéro</th>
						<th class="col-md-1">Nom</th>
						<th class="col-md-1">Adresse</th>
						<th class="col-md-1">Ville</th>
						<th class="col-md-1">Téléphone</th>
						<th class="col-md-1">Mail</th>
					</tr>
					<?php displayAllClients(); ?>
				</table>
				<br><br>
				<form action="../admin.php" method="post">
					<button type="submit" class="btn btn-default">Retour</button>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include '../footer.php';
	?>