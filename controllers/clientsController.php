<?php 
    include '../models/clientsModel.php';
    
    function displayAllClients(){
        $clients = selectAllClients();
        $data = $clients->fetchAll();
        foreach ($data as $line){
            echo "<tr>";
            echo "<td>".$line['Num']."</td>";
            echo "<td>".$line['Nom']."</td>";
            echo "<td>".$line['Adresse']."</td>";
            echo "<td>".$line['Ville']."</td>";
            echo "<td>".$line['Telephone']."</td>";
            echo "<td>".$line['Mail']."</td>";
            echo "</tr>";
        }
    }
    
    function createClient($nom, $adresse, $ville, $telephone, $mail){
        insertClient($nom, $adresse, $ville, $telephone, $mail);
    }
    
    function modifyClient($nom, $adresse, $ville, $telephone, $mail, $num){
        updateClient($nom, $adresse, $ville, $telephone, $mail, $num);
    }
    
    function removeClient($clients){        
        foreach ($clients as $numCli){
            deleteClient($numCli);
        }
    }
    
    function displayDropdownList(){
        $clients = selectAllClients();
        echo "<option name=\"client\" value=\"\"> </option>";
        foreach ($clients as $line){
            echo "<option name=\"client\" value=".$line['Num'].">".$line['Num']."</option>";
        }
        $clients->closeCursor();
    }
    
    function displayUpdateForm($num){
        $client = selectClient($num);
        $datas = $client->fetchAll();
        foreach ($datas as $data ){
            displayClientForm("value=".$data["Num"]." readonly", "value=".$data["Nom"], "value=".$data["Adresse"], "value=".$data["Ville"], "value=".$data["Telephone"], "value=".$data["Mail"]);
        }
    }
    
    function displayClientForm($num, $nom, $adresse, $ville, $telephone, $mail){
        echo "<label for=\"num\">Num</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"num\" ".$num.">";
        echo "<label for=\"nom\">Nom</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"nom\" ".$nom.">";
        echo "<label for=\"adresse\">Adresse</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"adresse\" ".$adresse.">";
        echo "<label for=\"ville\">Ville</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"ville\" ".$ville.">";
        echo "<label for=\"telephone\">T�l�phone</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"telephone\" ".$telephone.">";
        echo "<label for=\"mail\">Mail</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"mail\" ".$mail.">";
    }
    
    function displayClientsCheckbox(){           
        $res = selectAllClients();
        $data = $res->fetchAll();
        foreach ($data as $line){
            echo "<input type=\"checkbox\" name=\"client[]\" value=".$line['Num']."> ".$line['Num']."<br>";
        }
    }
?>