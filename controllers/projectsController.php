<?php 
    include '../models/projectsModel.php';
    
    function displayAllProjects(){
        $projects = selectAllProjects();
        $data = $projects->fetchAll();
        foreach ($data as $line){
            echo "<tr>";
            echo "<td>".$line['Num']."</td>";
            echo "<td>".$line['Nom']."</td>";
            echo "</tr>";
        }
    }
    
    function createProject($nom){
        insertProject($nom);
    }
    
    function modifyProject($nom, $num){
        updateProject($nom, $num);
    }
    
    function removeProject($projects){        
        foreach ($projects as $numCli){
            deleteProject($numCli);
        }
    }
    
    function displayDropdownList(){
        $projects = selectAllProjects();
        echo "<option name=\"project\" value=\"\"> </option>";
        foreach ($projects as $line){
            echo "<option name=\"project\" value=".$line['Num'].">".$line['Num']."</option>";
        }
        $projects->closeCursor();
    }
    
    function displayUpdateForm($num){
        $project = selectProject($num);
        $datas = $project->fetchAll();
        foreach ($datas as $data ){
            displayProjectForm("value=".$data["Num"]." readonly", "value=".$data["Nom"]);
        }
    }
    
    function displayProjectForm($num, $nom){
        echo "<label for=\"num\">Num</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"num\" ".$num.">";
        echo "<label for=\"nom\">Nom</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"nom\" ".$nom.">";
    }
    
    function displayProjectsCheckbox(){           
        $res = selectAllProjects();
        $data = $res->fetchAll();
        foreach ($data as $line){
            echo "<input type=\"checkbox\" name=\"project[]\" value=".$line['Num']."> ".$line['Num']."<br>";
        }
    }
?>