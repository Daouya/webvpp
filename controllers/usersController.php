<?php 
    include '../models/usersModel.php';
    
    function displayAllUsers(){
        $users = selectAllUsers();
        $data = $users->fetchAll();
        foreach ($data as $line){
            echo "<tr>";
            echo "<td>".$line['Login']."</td>";
            echo "<td>".$line['Nom']."</td>";
            echo "<td>".$line['Prenom']."</td>";
            echo "<td>".$line['Type']."</td>";
            echo "</tr>";
        }
    }
    
    function createUser($login, $nom, $prenom, $type, $password){
        insertUser($login, $nom, $prenom, $type, $password);
    }
    
    function modifyUser($nom, $prenom, $type, $password, $login){
        updateUser($nom, $prenom, $type, $password, $login);
    }
    
    function removeUser($users){
        foreach ($users as $login){
            deleteUser($login);
        }
    }
    
    function displayDropdownList(){
        $users = selectAllUsers();
        echo "<option name=\"user\" value=\"\"> </option>";
        foreach ($users as $line){
            echo "<option name=\"user\" value=".$line['Login'].">".$line['Login']."</option>";
        }
        $users->closeCursor();
    }
    
    function displayUpdateForm($login){
        $user = selectUser($login);
        $datas = $user->fetchAll();
        foreach ($datas as $data ){
            displayUserForm("value=".$data["Login"]." readonly", "value=".$data["Nom"], "value=".$data["Prenom"], "value=".$data["Type"], "value=".$data["Password"], true);
        }
    }
    
    function displayUserForm($login, $nom, $prenom, $type, $password, $scrollList){
        echo "<label for=\"login\">Login</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"login\" ".$login.">";
        echo "<label for=\"nom\">Nom</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"nom\" ".$nom.">";
        echo "<label for=\"prenom\">Prenom</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"prenom\" ".$prenom.">";
        echo "<label for=\"type\">Type</label>";
        if ($scrollList){
            echo "<select name=\"type\" id=\"type\">";
            echo "<option name=\"type\" value=\"'.$type.'\">".$type."</option>";
            echo "<option name=\"type\" value=\"Rapporteur\">Rapporteur</option>";
            echo "<option name=\"type\" value=\"Developpeur\">Développeur</option>";
            echo "</select>";
        } else {
            echo "<input type=\"text\" class=\"form-control\" name=\"type\" ".$type.">";            
        }
        echo "<label for=\"password\">Password</label>";
        echo "<input type=\"password\" class=\"form-control\" name=\"password\" ".$password.">";
    }
    
    function displayUsersCheckbox(){
        $res = selectAllUsers();
        $data = $res->fetchAll();
        foreach ($data as $line){
            echo "<input type=\"checkbox\" name=\"user[]\" value=".$line['Login']."> ".$line['Login']."<br>";
        }
    }
?>