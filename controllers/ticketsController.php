<?php
    include '../models/ticketsModel.php';
    include '../models/usersModel.php';
    include '../models/clientsModel.php';
    include '../models/projectsModel.php';
    
    function displayAllTickets(){
        $tickets = selectAllTickets();
        $data = $tickets->fetchAll();
        foreach ($data as $line){
            echo "<tr>";
            echo "<td>".$line['Num']."</td>";
            echo "<td>".$line['Details']."</td>";
            echo "<td>".$line['Signalement']."</td>";
            echo "<td>".$line['NumClient']."</td>";
            echo "<td>".$line['NumProjet']."</td>";
            echo "</tr>";
        }
    }
    
    function displayTicket($num){
        $ticket = selectTicket($num);
        $data = $ticket->fetchAll();
        foreach ($data as $line){
            echo "<tr>";
            echo "<td>".$line['Num']."</td>";
            echo "<td>".$line['Details']."</td>";
            echo "<td>".$line['Signalement']."</td>";
            echo "<td>".$line['NumClient']."</td>";
            echo "<td>".$line['NumProjet']."</td>";
            echo "</tr>";
        }
    }
    
    function createTicket($details, $signalement, $rapporteur, $client, $project){
        insertTicket($details, $signalement, $rapporteur, $client, $project);
        $num = selectLastInsert();        
        $datas = $num->fetchAll();        
        foreach ($datas as $data ){
            $ticket = selectTicket($data['Num']);
        }
        $datas = $ticket->fetchAll();
        foreach ($datas as $data ){            
            insertHistoric($data['Num'], $data['DateCreation'], "A faire", "", $data['LoginUtilisateur']);
        }
    }
    
    function modifyTicketState($num, $statut, $commentaire, $developpeur){
        insertHistoric($num, date("Y-m-d H:i:s", time() + (60 * 60 *2)), $statut, $commentaire, $developpeur);
    }
    
    function displayDropdownList(){
        $tickets = selectAllTickets();
        echo "<option name=\"ticket\" value=\"\"> </option>";
        foreach ($tickets as $line){
            echo "<option name=\"ticket\" value=".$line['Num'].">".$line['Num']."</option>";
        }
        $tickets->closeCursor();
    }
    
    function displayRapporteursDropdownList(){
        $users = selectAllRapporteurs();
        echo "<option name=\"rapporteur\" value=\"\"> </option>";
        foreach ($users as $line){
            echo "<option name=\"rapporteur\" value=".$line['Login'].">".$line['Login']."</option>";
        }
        $users->closeCursor();
    }
    
    function displayClientsDropdownList(){
        $clients = selectAllClients();
        echo "<option name=\"client\" value=\"\"> </option>";
        foreach ($clients as $line){
            echo "<option name=\"client\" value=".$line['Num'].">".$line['Num']."</option>";
        }
        $clients->closeCursor();
    }
    
    function displayProjectsDropdownList(){
        $projects = selectAllProjects();
        echo "<option name=\"project\" value=\"\"> </option>";
        foreach ($projects as $line){
            echo "<option name=\"project\" value=".$line['Num'].">".$line['Num']."</option>";
        }
        $projects->closeCursor();
    }
    
    function displayUpdateForm($num){
        $ticket = selectTicket($num);
        $datas = $ticket->fetchAll();
        foreach ($datas as $data ){
            displayTicketForm("value=".$data["Num"]." readonly", "", "", ""/*"value=".$_SESSION['user']." readonly"*/);
        }
    }
    
    function displayTicketForm($num, $statut, $commentaire, $developpeur){
        echo "<label for=\"num\">Num</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"num\" ".$num.">";
        echo "<label for=\"statut\">Statut</label>";
        echo "<select name=\"statut\" id=\"statut\" ".$statut.">";
        echo "<option name=\"statut\" value=\"\"></option>";
        echo "<option name=\"statut\" value=\"En cours\">En cours</option>";
        echo "<option name=\"statut\" value\"Termine\">Terminé</option>";
        echo "</select><br>";
        echo "<label for=\"commentaire\">Commentaire</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"commentaire\" ".$commentaire.">";
        echo "<label for=\"developpeur\">Développeur</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"developpeur\" ".$developpeur.">";
    }
    
    function displayTicketHistoric($num){
        $ticket = selectTicketHistoric($num);
        $datas = $ticket->fetchAll();
        foreach ($datas as $data ){
            displayTicketForm("value=".$data["Num"]." readonly", "", "", ""/*"value=".$_SESSION['user']." readonly"*/);
        }
    }
    
    function displayHistoric($num, $statut, $commentaire, $developpeur){
        echo "<label for=\"num\">Num</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"num\" ".$num.">";
        echo "<label for=\"statut\">Statut</label>";
        echo "<select name=\"statut\" id=\"statut\" ".$statut.">";
        echo "<option name=\"statut\" value=\"\"></option>";
        echo "<option name=\"statut\" value=\"En cours\">En cours</option>";
        echo "<option name=\"statut\" value\"Termine\">Terminé</option>";
        echo "</select><br>";
        echo "<label for=\"commentaire\">Commentaire</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"commentaire\" ".$commentaire.">";
        echo "<label for=\"developpeur\">Développeur</label>";
        echo "<input type=\"text\" class=\"form-control\" name=\"developpeur\" ".$developpeur.">";
    }
?>